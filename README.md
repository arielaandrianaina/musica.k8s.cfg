# MUSICA : CONFIGURATION DE DEPLOIEMENT
Les fichier de configuration pour lancer l'application Musica en dehors des fichiers de deploiement des microservices.
- Service : Ils seront en <b>ClusterIP</b>
# NAMESPACE : DEVELOPPEMENT
Creation d'un namespace pour l'application
- namespace.yml
***
Creation d'un quota pour le namespace 
- namespace-quota.yml
# BASE DE DONNE
Chaque microservice à ça propre base de donné qui est plus conforme à une architecure microservice
## MySQL    : Pour les utilisateurs
Cette base de donnée sera utilisé pour stocker les informations de l'utilisateur , qui elle sera utilisé par <b>[Musica-Asp](https://gitlab.com/mahatokyhasina25/musica.asp)</b>.
***
Deploiement :
- mysql-deploy.yml
***
Service de communication :
- mysql-service.yml
## MySQL    : Pour la liste des playliste des utilisateurs
Cette base de donnée sera utilisé pour stocker les informations des playlist créer par l'utilisateur , qui elle sera utilisé par <b>[Musica-Spring](https://gitlab.com/mahatokyhasina25/musica.spring)</b>.
***
Deploiement :
- mysql-deploy-spring.yml
***
Service de communication :
- mysql-service-spring.yml
## MongoDb  : Pour la liste des musiques et albums 
Cette base de donnée sera utilisé pour stocker les informations des albums et musiques , qui elle sera utilisé par <b>[Musica-Nest](https://gitlab.com/mahatokyhasina25/musica.nest)</b>.</br>
Elle est plus facile à manipuler pour ce cas , car le changement de donné peut varier.

***
Deploiement :
- mongo-deploy.yml
***
Service de communication :
- mongo-service.yml

# TEST 
On peut test le service par l'utilisation d'un pod de debug qui est en ALPINE :
- debug_pod.yaml

# Utilisation 
Pour utiliser et mettre en place c'est fichier de configuration , il suffit d'utiliser la commande <b>kubectl</b> :
#### appliquer le fichier
```
kubectl apply -f name_file.yml
```
#### supprimer les ressources creer par le fichier
```
kubectl delete -f name_file.yml
```
![IMAGE_DESCRIPTION](picture.png)
